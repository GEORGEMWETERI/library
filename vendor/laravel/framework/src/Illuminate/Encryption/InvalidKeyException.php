<?php namespace Illuminate\Encryption;
use Laravel\LegacyEncrypter\McryptEncrypter;

class InvalidKeyException extends \InvalidArgumentException {}
